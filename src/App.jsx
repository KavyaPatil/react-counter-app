import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import CounterApp from "./Components/CounterApp";

function App() {
  return (
    <>
    <div className="app">
      <CounterApp />
      </div>
    </>
  );
}

export default App;
