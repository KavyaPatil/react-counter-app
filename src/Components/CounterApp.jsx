import React, { useState } from "react";
import "./CounterApp.css";
import Cart from "./Cart/Cart";
import Header from "./Header/Header";
import Reset from "./ResetSection/Reset";

const items = [
  { quantity: 0, id: 1 },
  { quantity: 0, id: 2 },
  { quantity: 0, id: 3 },
  { quantity: 0, id: 4 },
];


const CounterApp = () => {
  const [cart, setCart] = useState([
    { quantity: 0, id: 1 },
    { quantity: 0, id: 2 },
    { quantity: 0, id: 3 },
    { quantity: 0, id: 4 },
  ]);

  const count = cart.reduce((total, item) => {
    // console.log(total,item.quantity > 0)
    if (item.quantity > 0) {
      total += 1;
      return total;
    } else {
      return total;
    }
  }, 0);


  const addHandler = (id) => {
    const newCart = cart.map((item) => {
      if (item.id === id) {
        const quantity = item.quantity + 1;
        return { ...item, quantity: quantity };
      } else {
        return item;
      }
    });

    setCart(newCart);
  };

  const deleteHandler = (id) => {
    const newCart = cart.filter((item) => {
      return id !== item.id;
    });

    setCart(newCart);
  };

  const minusHandler = (id) => {
    const newCart = cart.map((item) => {
      if (item.id === id) {
        return { ...item, quantity: --item.quantity };
      } else {
        return item;
      }
    });

    setCart(newCart);
  };

  const refreshHandler = () => {
    const newCart = cart.map((item) => {
      return { ...item, quantity: 0 };
    });

    setCart(newCart);
  };

  const recycleHandler = () => {
    const newCart = [...items];
    setCart(newCart);
  };

  return (
    <div className="container">
      <Header count={count} />
      <Reset refreshHandler={refreshHandler} recycleHandler={recycleHandler} length={cart.length} />
      {cart.map((item) => {
        return (
          <Cart
            quantity={item.quantity}
            key={item.id}
            addHandler={() => {
              addHandler(item.id);
            }}
            deleteHandler={() => {
              deleteHandler(item.id);
            }}
            minusHandler={() => {
              minusHandler(item.id);
            }}
          />
        );
      })}
    </div>
  );
};

export default CounterApp;
