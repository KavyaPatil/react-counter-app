import React from 'react';
import RefreshButton from "../UI/Button/RefreshButton/RefreshButton";
import RecycleButton from "../UI/Button/RecycleButton/RecycleButton";
import "./Reset.css"


const Reset = ({refreshHandler, recycleHandler, length}) => {
  return (
    <div className='flex'>
      <RefreshButton refreshHandler = {refreshHandler} length = {length}/>
      <RecycleButton recycleHandler = {recycleHandler} length = {length}/>
    </div>
  )
}

export default Reset