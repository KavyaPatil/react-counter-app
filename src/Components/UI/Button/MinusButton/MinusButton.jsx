import React from 'react';
import "./MinusButton.css";

const MinusButton = ({minusHandler}) => {
  return (
    <button className='blue' onClick={minusHandler}><i className="fa-solid fa-circle-minus" style={{color: "#ffffff"}}></i></button>
  )
}

export default MinusButton;