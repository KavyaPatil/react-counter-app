import React from 'react';
import "./RecycleButton.css";

const RecycleButton = ({recycleHandler, length}) => {

  return (
    <button className={length > 0 ? "blue" : "darkblue"} onClick={recycleHandler}><i className="fa-solid fa-recycle" style={{color: "#ffffff"}}></i>
    </button>
  )
}

export default RecycleButton