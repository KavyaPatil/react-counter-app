import React from 'react';
import "./AddButton.css"

const AddButton = ({addHandler}) => {
    
  return (
    <button className='gray' onClick={addHandler}><i className="fa-solid fa-circle-plus" style={{color: "#ffffff"}}></i></button>
  )
}

export default AddButton