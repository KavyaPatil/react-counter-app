import React from 'react';
import "./RefreshButton.css"

const RefreshButton = ({refreshHandler, length}) => {
  return (
    <button className= {length > 0 ? "green" : "lightgreen"} onClick={refreshHandler}><i className="fa-solid fa-arrows-rotate" style={{color: "#ffffff"}}></i></button>
  )
}

export default RefreshButton;