import React from 'react';
import "./DeleteButton.css"

const DeleteButton = ({deleteHandler}) => {
  return (
    <button className='red' onClick={deleteHandler}><i className="fa-regular fa-trash-can" style={{color: "#ffffff"}}></i></button>
  )
}

export default DeleteButton