import React from 'react'
import AddButton from '../UI/Button/AddButton/AddButton';
import MinusButton from '../UI/Button/MinusButton/MinusButton';
import DeleteButton from '../UI/Button/DeleteButton/DeleteButton';
import "./Cart.css"

const Cart = ({quantity, addHandler, deleteHandler, minusHandler}) => {

const Quantity = "Zero";

  return (
    <div className='flex'>
      <div className={quantity > 0 ? "blue" : "yellow"}>{quantity > 0 ? quantity : "Zero"}</div>
       <AddButton addHandler = {addHandler}/>
       <MinusButton minusHandler = {minusHandler}/>
       <DeleteButton deleteHandler = {deleteHandler}/>
    </div>
  )
}

export default Cart;